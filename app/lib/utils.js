define([
  'backbone',
  'marionette',
  'd3',
  'underscore'
], function (Backbone, Marionette, d3, _) {

  var utils = {};

  utils.defaultColor = function () {
    var colors = d3.scale.category20().range();
    return function (d, i) {
      return d.color || colors[i % colors.length];
    }
  };

  utils.getColor = function (color) {
    if (!arguments.length) return utils.defaultColor();

    if (_.isArray(color)) {
      return function (d, i) {
        return d.color || color[i % color.length];
      }
    } else {
      return color;
    }
  };

  utils.windowResize = function (fun) {
    var oldresize = window.onresize;

    window.onresize = function (e) {
      if (typeof oldresize == 'function') oldresize(e);
      if (typeof fun == 'function') fun(e);
    }
  };

  utils.calcApproxTextWidth = function (svgTextElem) {
    if (svgTextElem instanceof d3.selection) {
      var fontSize = parseInt(svgTextElem.style("font-size").replace("px", ""));
      var textLength = svgTextElem.text().length;

      return textLength * fontSize * 0.5;
    }
    return 0;
  };

  utils.windowSize = function () {
    // Sane defaults
    var size = {width: 640, height: 480};

    // Earlier IE uses Doc.body
    if (document.body && document.body.offsetWidth) {
      size.width = document.body.offsetWidth;
      size.height = document.body.offsetHeight;
    }

    // IE can use depending on mode it is in
    if (document.compatMode == 'CSS1Compat' &&
      document.documentElement &&
      document.documentElement.offsetWidth) {
      size.width = document.documentElement.offsetWidth;
      size.height = document.documentElement.offsetHeight;
    }

    // Most recent browsers use
    if (window.innerWidth && window.innerHeight) {
      size.width = window.innerWidth;
      size.height = window.innerHeight;
    }
    return (size);
  };

  utils.callChart = function (svg, chart, isPhantom) {
    svg = isPhantom ? svg : svg.transition();
    if (_.isArray(chart)){
      return svg.call.apply(svg, chart);
    } else {
      return svg.call(chart);
    }
  };


  return utils;
});
