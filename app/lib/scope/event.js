define(['marionette', 'underscore'], function(Marionette, _) {

    return Marionette.Object.extend({
        _isStoppedPropagation: false,

        initialize: function(options) {
            _.extend(this, _.pick(options, 'args', 'event', 'isIncludingCurrentScope'));
        },

        stopPropagation: function() {
            this._isStoppedPropagation = true;
        },

        isStoppedPropagation: function() {
            return this._isStoppedPropagation;
        }
    });
});