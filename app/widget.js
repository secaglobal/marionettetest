define(['marionette', 'service/scope', 'underscore'], function(Marionette, initialScope, _) {
    var lastId = 0;

    return Marionette.Widget.extend({
        widgets: null,
        _childrenWidgetsConfig: null,

        initialize: function(options) {
            options = options || {};

            if (!options.scope) {
                this._initiateScope();
            } else {
                this.scope = options.scope;
            }

            this.$id = ++lastId;
            this.alias = options.alias || ('widget_' + this.$id);

            if (!this.scope.namedWidgets) {
                this.scope.namedWidgets = {};
            }

            this.scope.namedWidgets[this.alias] = this;

            if (options.widgets) {
                this.initChildrenWidgets(options.widgets);
            }
        },

        _initiateScope: function() {
            this.scope = initialScope.$new();

            this.scope.$on('set:global', function(e, key, value) {
                e.stopPropagation();
                this.scope[key] = value;

                this.scope.$broadcast('set:global', key, value);
                this.scope.$broadcast('set:global:' + key, value);
            }.bind(this));

            this.scope.$on('broadcast:global', function(e) {
                e.stopPropagation();

                this.scope.$broadcast.apply(this.scope, e.args);
            }.bind(this));
        },

        initChildrenWidgets: function(widgetsConfig) {
            this.widgets = [];

            if (!widgetsConfig || !(widgetsConfig instanceof Object)) {
                return;
            }

            widgetsConfig.map(function(widgetConfig) {
                if (!widgetConfig.widget) {
                    return console.error('Widget\'s config has no widget property', widgetConfig);
                }

                var widgetPath = 'widget/' + widgetConfig.widget.replace(/-/g, '/'),
                    newScope = this.scope.$new(_.omit(widgetConfig, 'widgets', 'alias'));

                newScope.$stopEventBroadcasting();

                require([widgetPath], function(widgetClass) {
                    this.addChildrenWidget(new widgetClass({
                        scope: newScope,
                        widgets: widgetConfig.widgets || false,
                        alias: widgetConfig.alias || false
                    }));

                    newScope.$startEventBroadcasting();
                }.bind(this));
            }.bind(this));

            return this;
        },

        addChildrenWidget: function(widget) {
            this.widgets.push(widget);
        }
    });
});