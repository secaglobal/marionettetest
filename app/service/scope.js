define(['underscore', 'lib/scope/event'], function(_, ScopeEvent) {
    var lastIdNr = 0;

    return {
        $children: [],
        $_events: [],
        $_broadcastPool: [],
        $_broadcastingHandler: null,

        $initialize: function() {
            this.$id = ++lastIdNr;
            this.$children = [];
            this.$_events = {};
            this.$_broadcastPool = [];

            this.$startEventBroadcasting();
        },

        $new: function(attrs) {
            Scope.prototype = this;

            var scope = new Scope();

            scope.$parentScope = this;
            scope.$initialize();

            if (attrs) {
                _.extend(scope, attrs);
            }

            this.$children.push(scope);

            console.info('Created new scope', scope);

            return scope;

            function Scope() {};
        },

        $stopEventBroadcasting: function() {
            this.$setBroadcastHandler(this.$_storeBroadcast.bind(this))
        },

        $startEventBroadcasting: function() {
            this.$flushBroadcastingPool();
            this.$setBroadcastHandler(this.$_broadcastImmediately.bind(this));
        },

        $on: function(event, fn) {
            if (!this.$_events[event]) {
                this.$_events[event] = [];
            }

            this.$_events[event].push(fn);
        },

        $broadcast: function(eventCnf) {
            this.$getBroadcastHandler().call(null, new ScopeEvent({
                event: !eventCnf.event ? eventCnf : eventCnf.event,
                args: Array.prototype.slice.call(arguments, 1),
                isIncludingCurrentScope: eventCnf.isIncludingCurrentScope
            }));
        },

        $emit: function(eventCnf) {
            var event = new ScopeEvent({
                event: !eventCnf.event ? eventCnf : eventCnf.event,
                args: Array.prototype.slice.call(arguments, 1),
                isIncludingCurrentScope: eventCnf.isIncludingCurrentScope
            })


            console.info('Raised emit event:' + event.event, event, this);

            if (event.isIncludingCurrentScope && this.$_events[event.event]) {
                this.$_events[event.event].map(function(fn) {
                    console.info('Called event listener:', event);

                    fn.apply(null, [event].concat(event.args));
                });
            }

            if (!event.isStoppedPropagation() && this.$parentScope) {
                this.$parentScope.$emit.apply(this.$parentScope,
                    [{
                        event: event.event,
                        isIncludingCurrentScope: true
                    }].concat(event.args)
                );
            }
        },

        $setBroadcastHandler: function(fn) {
            this.$_broadcastingHandler = fn;
        },

        $getBroadcastHandler: function() {
            return this.$_broadcastingHandler;
        },

        $_broadcastImmediately: function(event) {
            console.info('Raised broadcast event:' + event.event, event, this);

            if (event.isIncludingCurrentScope && this.$_events[event.event]) {
                this.$_events[event.event].map(function(fn) {
                    console.info('Called event listener:', event);

                    fn.apply(null, [event].concat(event.args));
                });
            }

            if (!event.isStoppedPropagation()) {
                this.$children.map(function (scope) {
                    scope.$broadcast.apply(scope, [{
                        event: event.event,
                        isIncludingCurrentScope: true
                    }].concat(event.args));
                });
            }
        },

        $_storeBroadcast: function(event) {
            console.info('Stored broadcast event: ' + event.event, event, this);

            this.$_broadcastPool.push(event);
        },

        $flushBroadcastingPool: function() {
            while(this.$_broadcastPool.length) {
                this.$_broadcastImmediately.call(this, this.$_broadcastPool.shift());
            }
        }
    }
});