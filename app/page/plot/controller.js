define(['app', 'marionette', './view'], function (App, Marionette, View) {
    return Marionette.Controller.extend({
        index: function() {
            App.execute('show', 'main', new View());
        }
    });
});