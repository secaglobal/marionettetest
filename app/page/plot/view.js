define(['marionette', 'd3', 'underscore', 'text!./template.html'], function (Marionette, d3, _, template) {
    return Marionette.LayoutView.extend({
        className: 'container',

        render: function() {
            this.$el.append(new Marionette.TemplateCache().compileTemplate(template));

            require(['extra/plot-samples/setting1', 'widget/plot/canvas'], function(config, Canvas) {
                d3.json('app/extra/plot-samples/data1.json', function(err, data) {
                    if (err) {
                        return console.error('Data loading error', err);
                    }

                    var data = data.data.rows.slice(0, 20);
                    var canvas = new Canvas(config);

                    var datasets = {
                        names: data.map(function(row) { return row[0][0];}),
                        dataset1: data.map(function(row) { return row[0][1];}),
                        dataset2: data.map(function(row) { return row[0][0];}),
                        dataset3: data.map(function(row) { return {x: row[0][0], y: row[0][1]};}),
                        dataset4: data.map(function(row) { return {x: row[0][0], y: row[0][1] + 2};})
                    }

                    canvas
                        .setData(datasets)
                        .render(this.$el.find('.plot-block').get(0));

                    window.onresize = function() {
                        canvas.render(this.$el.find('.plot-block').get(0));
                    }.bind(this);
                }.bind(this));
            }.bind(this));
        }
    });
});