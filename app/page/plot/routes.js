define(['marionette', './controller'], function (Marionette, Controller) {
    new Marionette.AppRouter({
        controller: new Controller(),
        appRoutes: {
            "*path": "index"
        }
    });
});