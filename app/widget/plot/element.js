define(['../../widget'], function(Widget) {
    return Widget.extend({
        initialize: function() {
            Widget.prototype.initialize.apply(this, arguments);

            this.scope.region = {}
            this.margin = {top: 0, left: 0, right: 0, bottom: 0};
            this.scope.width = 0;
            this.scope.height = 0;

            this._traverseScopeParamsHandlers();
            this._prepareMargines();
            this._recalculateRegionResolution();

            this.scope.$on('render', this.onRender.bind(this));
        },

        // Warning: Should use widget fields config instead scope.
        _traverseScopeParamsHandlers: function() {
            var iterator = 0;

            for (var option in this.scope) {
                if (this['_' + option + 'Handler']) {
                    this['_' + option + 'Handler']();
                }

                iterator++;
            }

            if (iterator >= 100) {
                console.error('Scope has too many keys: ' + iterator);
            }

            console.info('Scope keys amount: ' + iterator);
        },

        setData: function(data) {
            this.scope.data = data;
            return this;
        },

        render: function() {
            this._recalculateRegionResolution();
        },

        onRender: function(event, el) {
            console.info('Executed onRender: ' + this.scope.widget);

            event.stopPropagation();

            this.render(el);
        },

        _recalculateRegionResolution: function() {
            if (!this.scope.$parentScope.region) {
                return;
            }

            this.scope.region = {
                width: this.scope.$parentScope.region.width - this.margin.left - this.margin.right,
                height: this.scope.$parentScope.region.height - this.margin.top - this.margin.bottom
            }
        },

        _prepareMargines: function() {
            if (this.scope.margin) {
                _.extend(this.margin, this.scope.margin);
                delete this.scope.margin;
            }
        }
    });
});