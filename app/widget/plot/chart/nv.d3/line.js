define(['../../element', '../../../../lib/nvd3/line'], function(Element, lineChart) {
    return Element.extend({
        render: function(el) {
            console.info('Line rendering');

            Element.prototype.render.apply(this, arguments);

            var height = this.scope.region.height,
                width = this.scope.region.width,
                data = this.scope.data;

            if (this.scope.dataset) {
                data = data[this.scope.dataset];
            }

            if (!data || !data.length) {
                return console.error('No data');
            }

            var chart = lineChart()
                .width(width)
                .height(height)
                .margin(this.margin)
                .interpolate(this.scope.interpolate || 'linear')
                .color(function() { return this.scope.stroke || 'steelblue'; }.bind(this));

            if (this.scope.yDomain) {
                chart.yDomain(this.scope.yDomain);
            }

            el.datum([{values: data}]).call(chart);

            this.scope.$broadcast('render', chart);
        }
    });
});