define(['../element'], function(Element) {
    return Element.extend({
        initialize: function() {
            Element.prototype.initialize.apply(this, arguments);

            this.scope.$on('zoom', function() {
                this.render();
            }.bind(this));
        },

        render: function(el) {
            console.info('Line rendering');

            this.el = el || this.el;

            Element.prototype.render.apply(this, arguments);

            var height = this.scope.region.height,
                width = this.scope.region.width,
                data = this.scope.data;

            if (this.scope.dataset) {
                data = data[this.scope.dataset];
            }

            if (!data || !data.length) {
                return console.error('No data');
            }

            if (!this.scope.yScale) {
                return console.error('No yScale');
            }

            var xScale = d3.scale.ordinal()
                .domain(data.map(function(d) { return d.x; }))
                .rangeBands([0, width], 0.1);

            var yScale = this.scope.yScale.range([height, 0]);

            var lineContainer = this.el.select('#plot-line-' + this.$id);

            if (lineContainer.empty()) {
                lineContainer = this.el.append('g').attr('id', 'plot-line-' + this.$id);
            }

            this.line = d3.svg.line()
                .interpolate(this.scope.interpolate || 'linear')
                //.defined(defined)
                .x(function(d) { return xScale(d.x) + xScale.rangeBand() / 2; })
                .y(function(d) { return yScale(d.y); });

            this.linePaths = lineContainer.selectAll('path').data([data]);

            this.linePaths.attr('d', this.line);

            this.linePaths.enter().append('path')
                .attr('fill', this.scope.fill || 'none')
                .attr('stroke', this.scope.stroke || 'steelblue')
                .attr('stroke-width', this.scope.strokeWidth || '1px')

                .attr('d', this.line);

            this.scope.$broadcast('render', lineContainer);
        }
    });
});