define(['../element', 'd3'], function(Element, d3) {
    return Element.extend({
        initialize: function() {
            Element.prototype.initialize.apply(this, arguments);

            this.scope.$on('zoom', function() {
                this.render();
            }.bind(this));
        },

        render: function(el) {
            console.info('Bar rendering');

            this.el = el || this.el;

            Element.prototype.render.apply(this, arguments);

            var height = this.scope.region.height,
                width = this.scope.region.width,
                tensity = (this.scope.tensity || 1),
                data = this.scope.data,
                position = this.scope.position || 0;

            if (this.scope.dataset) {
                data = data[this.scope.dataset];
            }

            if (!data || !data.length) {
                return console.error('No data');
            }

            var xScale = d3.scale.ordinal()
                .domain(data.map(function(d, i) {return i;}))
                .rangeBands([0, width], 0.1);

            var yScale = this.scope.yScale
                .range([height, 0]);

            var barContainer = this.el.select('#plot-bar-' + this.$id),
                width = xScale.rangeBand() * tensity;

            if (barContainer.empty()) {
                 barContainer = this.el.append('g').attr('id', 'plot-bar-' + this.$id);
            }

            barContainer.attr('transform', 'translate(' + (width * position) + ',0)');

            var bars = barContainer.selectAll('.bar').data(data);

            bars
                .attr('width', width)
                .attr('height', function(d) { return  height - yScale(d); })
                .attr('x', function(d, i) { return xScale(i); })
                .attr('y', function(d) { return yScale(d); });


            bars.enter().append('rect')
                .attr('class', 'bar')
                .attr('fill', this.scope.fill)
                .attr('width', width)
                .attr('height', function(d) { return  height - yScale(d); })
                .attr('x', function(d, i) { return xScale(i); })
                .attr('y', function(d) { return yScale(d); });

            this.bars = bars;

            this.scope.$broadcast('render', bars);
        }
    });
});