define(['./element', 'jquery', 'd3', 'underscore'], function(Element, $, d3, _) {
    return Element.extend({
        initialize: function(options) {
            Element.prototype.initialize.apply(this, arguments);

            this.scope.canvas = this;
        },

        render: function(domEl) {
            this.width = this.scope.region.width = $(domEl).width();
            this.height = this.scope.region.height = $(domEl).height();

            var container = d3.select(domEl);
            var svg = container.select('svg');

            if (svg.empty()) {
                svg = container.append('svg')
                    .attr('width', "100%")
                    .attr('height', "100%");
            }

            this.scope.$broadcast('render', svg);

            return this;
        }
    });
});