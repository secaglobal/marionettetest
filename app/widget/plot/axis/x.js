define(['../element', 'd3', 'underscore'], function(Element, d3, _) {
    return Element.extend({
        render: function(el) {
            console.info('X axe rendering');

            Element.prototype.render.apply(this, arguments);

            var height = this.scope.region.height,
                width = this.scope.region.width,
                data = this.scope.data;

            if (this.scope.dataset) {
                data = data[this.scope.dataset];
            }

            var xDomain = data;
            var xScale = d3.scale.ordinal().domain(xDomain);
            var xRange = xScale.rangeBands([0, width], 0.1);
            var xAxis = d3.svg.axis().scale(xScale).orient('bottom');
            var axis = el.select('#axis-' + this.$id);

            if (axis.empty()) {
                axis = el.append('g').attr('id', 'axis-' + this.$id);
            }

            axis
                .attr('class', 'x axis')
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            this.scale = xScale;
            this.domain = xDomain;
            this.axis = xAxis;
            this.range = xRange;
            this.container = axis;

            this.scope.$broadcast('render', axis);
        }
    });
});