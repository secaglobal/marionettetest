define(['../element', 'd3', 'underscore'], function(Element, d3, _) {
    return Element.extend({
        initialize: function() {
            Element.prototype.initialize.apply(this, arguments);

            this.scope.$on('zoom', function() {
                this.container.call(this.axis)
            }.bind(this));
        },

        render: function(el) {
            console.info('Y axe rendering');

            Element.prototype.render.apply(this, arguments);

            var height = this.scope.region.height;

            if (!this.scope.yScale) {
                return console.error('No yScale');
            }

            var yScale = this.scope.yScale;
            var yRange = yScale.range([height, 0], 0.25);
            var yAxis = d3.svg.axis().scale(yRange).orient('left');

            var container = el.select('#axis-' + this.$id);

            if (container.empty()) {
                container = el.append('g').attr('id', 'axis-' + this.$id);

                if (this.scope.label) {
                    container
                        .attr('class', 'y axis')
                        .append("text")
                            .attr("transform", "rotate(-90)")
                            .attr("y", 6)
                            .attr("dy", ".71em")
                            .style("text-anchor", "end")
                            .text(this.scope.label)
                }
            }

            container.call(yAxis)

            this.container = container;
            this.axis = yAxis;

            this.scope.$emit('broadcast:global', 'widget:rendered:' + this.alias, this);
        }
    });
});