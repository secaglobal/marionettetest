define(['../element', 'd3', 'underscore'], function(Element, d3, _) {
    return Element.extend({

        initialize: function() {
            Element.prototype.initialize.apply(this, arguments);

            this.scope.layout = this;
        },

        processAxis: function() {
            _.map(this.scope.axis, function(data, lable) {
                var scaleName = lable + 'Scale',
                    scale = d3.scale[data.scale]();

                scale.type = data.scale;

                if (data.scale == 'linear') {
                    scale.domain(d3.extent(this.scope.data[data.dataset[0]]));
                } else if (data.scale == 'ordinal') {
                    scale.domain(this.scope.data[data.dataset[0]]);
                }

                this.scope[scaleName] = scale;
            }.bind(this));
        },

        render: function(container) {
            console.info('Layout rendering');
            this.container = container || this.container;

            Element.prototype.render.apply(this, arguments);

            var layout = this.container.select('#layout-' + this.$id);

            if (layout.empty()){
                layout = this.container.append('g')
                    .attr('id', 'layout-' + this.$id)
                    .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

                this.processAxis();
            }


            this.scope.$broadcast('render', layout);
        }
    });
});