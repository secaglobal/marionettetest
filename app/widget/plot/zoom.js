define(['./element', 'jquery', 'd3', 'widget/plot/axis/x', 'widget/plot/axis/y'], function(Element, $, d3, XAxis, YAxis) {
    return Element.extend({
        initialize: function() {
            Element.prototype.initialize.apply(this, arguments);

            this.zoom = d3.behavior.zoom()
                .scaleExtent([1, 10])
                .on("zoom", function() {
                    this.scope.$emit('broadcast:global', 'zoom');
                }.bind(this));
        },

        render: function(el) {
            console.log('render zoom');

            if (this.scope.yScale && this.scope.yScale.type == 'linear') {
                this.zoom.y(this.scope.yScale);
            }

            d3.select('svg').call(this.zoom);

            //this.scope.$broadcast('render', el);
            console.log(this);

            return this;
        }
    });
});