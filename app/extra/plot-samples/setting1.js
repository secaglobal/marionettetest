define(function() {
    return {
        widgets: [
            {
                widget: 'plot-layout-default',
                lineMode: "cardinal",

                axis: {
                    x: {
                        scale: 'ordinal',
                        dataset: ['names']
                    },
                    y: {
                        scale: 'linear',
                        dataset: ['dataset1', 'dataset2']
                    }
                },

                widgets: [
                    {
                        widget: 'plot-zoom',
                        useAxis: ['yAxis']
                    },
                    {
                        widget: 'plot-axis-y',
                        label: 'Y Axis',
                        dataset: 'dataset1',
                        alias: 'yAxis'
                    },
                    {
                        widget: 'plot-axis-x',
                        label: 'X Axis',
                        dataset: 'names',
                        alias: 'xAxis'
                    },
                    {
                        widget: 'plot-chart-bar',
                        label: "Bar1",
                        fill: "#1f77b4",
                        dataset: 'dataset1',
                        tensity: 0.5,
                        widgets: [
                            {
                                widget: 'tooltip'
                            }
                        ]
                    },
                    {
                        widget: 'plot-chart-bar',
                        label: "Bar2",
                        fill: "#ff7f0e",
                        dataset: 'dataset2',
                        tensity: 0.5,
                        position: 1
                    },
                    {
                        widget: 'plot-chart-line',
                        label: "Line1",
                        stroke: "#d62728",
                        strokeWidth: '2px',
                        dataset: 'dataset3'
                    },
                    {
                        widget: 'plot-chart-nv.d3-line',
                        label: "Line2",
                        stroke: '#2CA02C',
                        strokeWidth: '2px',
                        interpolate: 'cardinal',
                        dataset: 'dataset4'
                    }
                ],

                margin: {
                    top: 20, right: 20, bottom: 40, left: 40
                }
            },
        ]
    };
});