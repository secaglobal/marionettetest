requirejs.config({
    baseUrl: 'app/',
    paths: {
        jquery: '../bower_components/jquery/dist/jquery.min',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        marionette: '../bower_components/backbone.marionette/lib/backbone.marionette.min',
        handlebars: '../bower_components/handlebars/handlebars.amd.min',
        d3: '../bower_components/d3/d3',
        text: '../bower_components/requirejs-text/text',
        app: 'entrypoint/default/app'
    },
    shim: {
        backbone: { deps: ['jquery', 'underscore'] },
        marionette: {  deps: ['backbone'] }
    }
});

requirejs(['marionette', 'handlebars'], function (Marionette, Handlebars) {
    console.info = function() {};

    Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate) {
        return Handlebars.default.compile(rawTemplate);
    };

    Marionette.Widget = Marionette.Controller;

    require(['app'], function(App) {
        App.start();
    });
});