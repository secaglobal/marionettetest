define(['marionette'], function (Marionette) {
    var App = new Marionette.Application({
        regions: {
            mainRegion: 'body'
        },

        showViewInRegion: function(region, view) {
            this[region + 'Region'].show(view);
        }
    });

    App.addInitializer(function() {
        require(['entrypoint/default/routes'], function() {
            Backbone.history.start();
        });
    });

    App.commands.setHandler('show', App.showViewInRegion.bind(App));

    return App;
});